from visual import *
import numpy as np
import sys

options = dict(
    inverse='--inverse',
    cubes='--cubes',
    rectangles='--rectangles'
)


class Planet:
    def __init__(self, planet_name, planet_mass, distance_from_center, start_angle, radius, ellipse_a, ellipse_b, x, y,
                 z):
        self.G = 6.67 * 10 ** (-11)
        self.sun_mass = 1.989 * 10 ** 30
        self.earth_mass = 5.973 * 10 ** 24
        self.neptune_mass = 1.0243 * 10 ** 26

        self.planet_name = planet_name
        self.planet_mass = planet_mass
        self.distance_from_center = distance_from_center
        self.start_angle = start_angle
        self.radius = radius
        self.ellipse_a = ellipse_a
        self.ellipse_b = ellipse_b
        self.x = x
        self.y = y
        self.z = z

    def gravitational_force(self, center='sun'):
        if center == 'sun':
            return self.G * (self.planet_mass * self.sun_mass) / (self.distance_from_center ** 2)
        elif center == 'earth':
            return self.G * (self.planet_mass * self.earth_mass) / (self.distance_from_center ** 2)
        elif center == 'neptune':
            return self.G * (self.planet_mass * self.neptune_mass) / (self.distance_from_center ** 2)

    def angular_velocity(self, center='sun'):
        return (self.gravitational_force(center=center) /
                (self.planet_mass * self.distance_from_center)) ** 0.5

    def angular_position(self, in_time, center='sun'):
        return self.start_angle + self.angular_velocity(center=center) * in_time

    def angular_position_change(self, in_time, time_change_, center='sun'):
        return self.angular_position(in_time + time_change_, center=center) - \
               self.angular_position(in_time, center=center)

    def picture(self):
        parameters = {'pos': vector(self.x, self.y, self.z),
                      'radius': self.radius,
                      'length': 2 * self.radius,
                      'height': 2 * self.radius,
                      'width': 2 * self.radius}

        if self.planet_name == 'mercury':
            parameters['material'] = materials.wood
            parameters['color'] = (204, 153, 51)

        elif self.planet_name == 'venus':
            parameters['material'] = materials.wood
            parameters['color'] = (204, 51, 0)

        elif self.planet_name == 'earth':
            parameters['material'] = materials.earth

        elif self.planet_name == 'mars':
            parameters['material'] = materials.marble
            parameters['color'] = color.orange

        elif self.planet_name == 'jupiter':
            parameters['material'] = materials.marble

        elif self.planet_name == 'saturn':
            parameters['material'] = materials.wood

        elif self.planet_name == 'uranus':
            parameters['material'] = materials.rough
            parameters['color'] = color.cyan

        elif self.planet_name == 'neptune':
            parameters['material'] = materials.rough
            parameters['color'] = color.blue

        elif self.planet_name == 'moon':
            parameters['material'] = materials.rough

        if options['cubes'] in sys.argv:
            return box(**parameters)
        else:
            return sphere(**parameters)


def years_to_seconds(years):
    return years * 365 * 24 * 60 * 60


def rotate_ellipse(planet_img, angular_position, a, b, inverse=False):
    x = a * np.cos(angular_position)
    y = b * np.sin(angular_position)

    if inverse:
        y = -y

    planet_img.pos = vector(x, y, 0)


def rotate_rectangle(planet_img, angular_position, a, b, inverse=False):
    base_angle = np.arctan(a / b)
    position_normalized = angular_position % (2 * np.pi)

    if position_normalized < base_angle:
        x = a
        y = position_normalized / base_angle * b

    elif position_normalized < np.pi / 2:
        x = (1 - (position_normalized - base_angle) / (np.pi / 2 - base_angle)) * a
        y = b

    elif position_normalized < np.pi - base_angle:
        x = -(position_normalized - np.pi / 2) / (np.pi / 2 - base_angle) * a
        y = b

    elif position_normalized < np.pi:
        x = -a
        y = (1 - (position_normalized - np.pi + base_angle) / base_angle) * b

    elif position_normalized < np.pi + base_angle:
        x = -a
        y = -(position_normalized - np.pi) / base_angle * b

    elif position_normalized < 1.5 * np.pi:
        x = (-1 + (position_normalized - np.pi - base_angle) / (np.pi / 2 - base_angle)) * a
        y = -b
    elif position_normalized < 2 * np.pi - base_angle:
        x = (position_normalized - 1.5 * np.pi) / (np.pi / 2 - base_angle) * a
        y = -b
    else:
        x = a
        y = (-1 + (position_normalized + base_angle - 2 * np.pi) / base_angle) * b

    if inverse:
        y = -y

    planet_img.pos = vector(x, y, 0)


def make_sun():
    if options['cubes'] in sys.argv:
        box(pos=vector(3, 0, 0), color=color.yellow, length=6.0, height=6.0, width=6.0)
    else:
        sphere(pos=vector(3, 0, 0), color=color.yellow, radius=3.0)


make_sun()

planets = [Planet('mercury', 3.302 * 10 ** 23, 57910000000, 0, 0.3, 8.0, 6.0, 51.5, 0, 0),
           Planet('venus', 4.8685 * 10 ** 24, 108200000000, 0, 0.4, 12.0, 9.0, 53, 0, 0),
           Planet('earth', 5.973 * 10 ** 24, 149600000000, 0, 0.5, 16.0, 12.0, 55, 0, 0),
           Planet('mars', 6.4185 * 10 ** 23, 227900000000, 0, 0.45, 20.0, 15.0, 57, 0, 0),
           Planet('jupiter', 1.8986 * 10 ** 27, 778500000000, 0, .8, 24.0, 18.0, 59, 0, 0),
           Planet('saturn', 5.6846 * 10 ** 26, 1433000000000, 0, 0.7, 28.0, 21.0, 61, 0, 0),
           Planet('uranus', 8.6832 * 10 ** 25, 2877000000000, 0, 0.6, 32.0, 24.0, 63, 0, 0),
           Planet('neptune', 1.0243 * 10 ** 26, 4503000000000, 0, 0.6, 36.0, 27.0, 70, 0, 0)]

planets_images = [planet.picture() for planet in planets]

planets_object_image_pairs = zip(planets, planets_images)

moon_1 = Planet('moon', 7.347 * 10 ** 22, 384400000, 0, 0.2, None, None, 55 + 0.9, 0, 0)
moon_1_position = vector(0.9, 0, 0)
moon_1_picture = moon_1.picture()

moon_2 = Planet('moon', 2.14 * 10 ** 22, 3548000000, 0, 0.1, None, None, 70 + 1.0, 0, 0)
moon_2_position = vector(1.0, 0, 0)
moon_2_picture = moon_2.picture()

moon_3 = Planet('moon', 1.6 * 10 ** 17, 16681000000, 0, 0.2, None, None, 70 + 1.5, 0, 0)
moon_3_position = vector(1.5, 0, 0)
moon_3_picture = moon_3.picture()

how_many_seconds = years_to_seconds(1000)
current_time = 0
time_change = 10000

inverse_ = options['inverse'] in sys.argv

if options['rectangles'] in sys.argv:
    rotate_function = rotate_rectangle
else:
    rotate_function = rotate_ellipse

while current_time < how_many_seconds:
    rate(1000)

    counter = 0

    for planet_object, planet_image in planets_object_image_pairs:
        rotate_function(planet_image, planet_object.angular_position(current_time), planet_object.ellipse_a,
                        planet_object.ellipse_b, inverse=(counter % 2 != 0))

        if inverse_:
            counter += 1

    moon_1_position = rotate(moon_1_position,
                             angle=moon_1.angular_position_change(current_time, time_change, center='earth'),
                             axis=vector(0, 0, 1))
    moon_1_picture.pos = planets_images[2].pos + moon_1_position

    moon_2_position = rotate(moon_2_position,
                             angle=moon_2.angular_position_change(current_time, time_change, center='neptune'),
                             axis=vector(0, 0, 1))
    moon_2_picture.pos = planets_images[7].pos + moon_2_position

    moon_3_position = rotate(moon_3_position,
                             angle=moon_3.angular_position_change(current_time, time_change, center='neptune'),
                             axis=vector(0, 1, 0))
    moon_3_picture.pos = planets_images[7].pos + moon_3_position

    current_time += time_change
